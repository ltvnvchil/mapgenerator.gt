﻿namespace MapGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "Icon_terrain_coast.png");
            this.imageList.Images.SetKeyName(1, "Icon_terrain_desert.png");
            this.imageList.Images.SetKeyName(2, "Icon_terrain_desert_hills.png");
            this.imageList.Images.SetKeyName(3, "Icon_terrain_desert_mountain.png");
            this.imageList.Images.SetKeyName(4, "Icon_terrain_floodplains.png");
            this.imageList.Images.SetKeyName(5, "Icon_terrain_forest.png");
            this.imageList.Images.SetKeyName(6, "Icon_terrain_grass.png");
            this.imageList.Images.SetKeyName(7, "Icon_terrain_grass_hills.png");
            this.imageList.Images.SetKeyName(8, "Icon_terrain_grass_mountain.png");
            this.imageList.Images.SetKeyName(9, "Icon_terrain_ice.png");
            this.imageList.Images.SetKeyName(10, "Icon_terrain_jungle.png");
            this.imageList.Images.SetKeyName(11, "Icon_terrain_marsh.png");
            this.imageList.Images.SetKeyName(12, "Icon_terrain_oasis.png");
            this.imageList.Images.SetKeyName(13, "Icon_terrain_ocean.png");
            this.imageList.Images.SetKeyName(14, "Icon_terrain_outofplay.png");
            this.imageList.Images.SetKeyName(15, "Icon_terrain_plains.png");
            this.imageList.Images.SetKeyName(16, "Icon_terrain_plains_hills.png");
            this.imageList.Images.SetKeyName(17, "Icon_terrain_plains_mountain.png");
            this.imageList.Images.SetKeyName(18, "Icon_terrain_snow.png");
            this.imageList.Images.SetKeyName(19, "Icon_terrain_snow_hills.png");
            this.imageList.Images.SetKeyName(20, "Icon_terrain_snow_mountain.png");
            this.imageList.Images.SetKeyName(21, "Icon_terrain_tundra.png");
            this.imageList.Images.SetKeyName(22, "Icon_terrain_tundra_hills.png");
            this.imageList.Images.SetKeyName(23, "Icon_terrain_tundra_mountain.png");
            // 
            // pictureBox
            // 
            this.pictureBox.Location = new System.Drawing.Point(0, 1);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(2500, 2500);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Icon_terrain_desert_mountain.png");
            this.imageList1.Images.SetKeyName(1, "Icon_terrain_grass_mountain.png");
            this.imageList1.Images.SetKeyName(2, "Icon_terrain_plains_mountain.png");
            this.imageList1.Images.SetKeyName(3, "Icon_terrain_snow_mountain.png");
            this.imageList1.Images.SetKeyName(4, "Icon_terrain_tundra_mountain.png");
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(24, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1924, 1055);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.ImageList imageList1;
        public System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.Button button1;
    }
}

