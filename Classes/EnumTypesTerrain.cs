﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGenerator.Classes
{
    enum EnumTypesTerrain
    {
        coast=0, 
        desert=1,
        desert_hills=2,
        desert_mountains=3,  
        floodplains=4,
        forest=5,   
        grass=6,
        grass_hills=7,
        grass_mountain=8,   
        ice=9,   
        jungle=10,        
        marsh,
        oasis,   
        ocean,
        outofplay,
        plains=15,
        plains_hills=16,
        plains_mountaine=17,
        snow,
        snow_hills,
        snow_mountainice=20, 
        tundra=21,
        tundra_hills=22,
        tundra_mountain=23             
            
    }
}
