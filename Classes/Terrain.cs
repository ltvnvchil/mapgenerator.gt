﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGenerator.Classes
{
    class Terrain
    {
        private int width, height;
        private List<List<Hex>> Field = new List<List<Hex>>();

        public Terrain(int width, int height, int n )
        {
            this.width = width;
            this.height = height;
            for (int i = 0; i < height + n; i++)
            {
            Field.Add(new List<Hex>());   
                for (int j = 0; j < width + n; j++)
                {
                    Field[i].Add(new Hex());
                }
            }
            GenerationTerrain G = new GenerationTerrain();
            G.GenerationTerrainRandomHigh(ref Field, width, height);
        }

        public List<List<Hex>> getTerrain()
        {
            return Field;
        }

        public int getWidth()
        {
            return width;
        }

        public int getHeight()
        {
            return height;
        }
    }
}
