﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGenerator.Classes
{
    class GenerationTerrain
    {

        public List<List<Hex>> GenerationTerrainRandomHigh (ref List<List<Hex>> Terrain, int width, int height) {
            for (int i = 0; i < width+2; i++)
            {                
                Terrain[0][i].setRandomHigh(0);
                Terrain[height+1][i].setRandomHigh(0);
            }
            for(int i = 0; i < height+2; i++){
                Terrain[i][0].setRandomHigh(0);
                Terrain[i][width+1].setRandomHigh(0);
            }
            Random random = new Random();
            for (int i = 1; i < height+1; i++)
            {
                for (int j = 1; j < width+1; j++)
                {
                    int ran = random.Next(1,100);
                    Terrain[i][j].setRandomHigh(ran);
                }
            }
                return GenerateTerrainNormalize(ref Terrain, width, height);
        }

        public List<List<Hex>> DiamondSquareGenerator(ref List<List<Hex>> Terrain, int width, int height)
        {
            Random random = new Random();
            Terrain[0][0].setRandomHigh(random.Next(1, 100));
            Terrain[height][0].setRandomHigh(random.Next(1, 100));
            Terrain[0][width].setRandomHigh(random.Next(1, 100));

            return GenerateTerrainNormalize(ref Terrain, width, height);
        }

        public List<List<Hex>> GenerateTerrainNormalize(ref List<List<Hex>> Terrain, int width, int height)
        {
            for (int i = 1; i < height+1; i++)
            {   for (int j = 1; j < width+1; j++)
                {                  
                    int sum = Terrain[i][j].getRandomHigh() + Terrain[i][j - 1].getRandomHigh() + 
                        Terrain[i][j+1].getRandomHigh() + Terrain[i-1][j].getRandomHigh() + 
                        Terrain[i - 1][j-1].getRandomHigh() + Terrain[i+1][j].getRandomHigh() + 
                        Terrain[i + 1][j-1].getRandomHigh();

                    Terrain[i][j].setRandomHigh(sum/7);
                }
            }
            return SetPreTerrain(ref Terrain, width, height);
        }

        public List<List<Hex>> SetPreTerrain(ref List<List<Hex>> Terrain, int width, int height)
        {
            for (int i = 1; i < height+1; i++)
            {
                for (int j = 1; j < width+1; j++)
                {
                    Terrain[i][j].setType(Terrain[i][j].getRandomHigh());
                }
            }
            return Terrain;
        }   

        /*public int SetWidth(int num)
        {
            Width = num;
            return Width;
        }
        public int SetHeight(int num)
        {
            Height = num;
            return Height;
        }*/

    }
}
