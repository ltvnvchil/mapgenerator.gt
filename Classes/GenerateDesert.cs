﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapGenerator.Classes
{
    class GenerateDesert
    {
        //генератор пустыни
        public  GenerateDesert(ref  Terrain Terrain, int width, int height)
        {
            Random random = new Random();

            List<List<Hex>> Ter = Terrain.getTerrain();

            int ranX = 0, ranY = 0;
            while (Ter[ranX][ranY].getMainType() == true || Ter[ranX][ranY].getField() == false)
            {
                ranX = random.Next(height / 3, 2 * height / 3);
                ranY = random.Next(1, width);
            }

            Ter[ranX][ranY].setType(EnumTypesTerrain.desert);
            Ter[ranX][ranY].setMainType();
            Ter[ranX + 1][ranY + 1].setType(EnumTypesTerrain.desert);
            Ter[ranX + 1][ranY + 1].setMainType();
            Ter[ranX][ranY + 1].setType(EnumTypesTerrain.desert);
            Ter[ranX][ranY + 1].setMainType();
            Ter[ranX + 1][ranY].setType(EnumTypesTerrain.desert);
            Ter[ranX + 1][ranY].setMainType();
            Ter[ranX + 1][ranY + 2].setType(EnumTypesTerrain.desert_hills);
            Ter[ranX + 1][ranY + 2].setMainType();
            Ter[ranX + 2][ranY + 2].setType(EnumTypesTerrain.desert_hills);
            Ter[ranX + 2][ranY + 2].setMainType();
            for (int c = 0; c < 3; c++)
            {
                for (int i = height / 3; i < height / 3 * 2; i++)
                {
                    
                    int j = 1, j1 = width, count = 1;
                    if (c % 2 != 0)
                    {
                        j = width;
                        j1 = 1;
                        count = -1;
                    }
                    for (; j != j1 - count*2; j+=count)
                    {
                        if (Ter[i][j].getMainType() == false && Ter[i][j].getField() == true)
                        {
                            int sum = 10;
                            bool trueLand = false;
                            for (int k = i - 1; k < i + 2; k++)
                                for (int h = j - 1; h < j + 2; h++)
                                {
                                    if ((EnumTypesTerrain)Ter[k][h].getType() == EnumTypesTerrain.desert ||
                                        (EnumTypesTerrain)Ter[k][h].getType() == EnumTypesTerrain.desert_hills ||
                                        (EnumTypesTerrain)Ter[k][h].getType() == EnumTypesTerrain.desert_mountains ||
                                        (k == i && h == j))
                                    {
                                        sum += 16;
                                        trueLand = true;
                                    }
                                }
                            /*int sum = (int)Ter[i][j].getType() + (int)Ter[i][j - 1].getType() +
                            (int)Ter[i][j + 1].getType() + (int)Ter[i - 1][j].getType() +
                            (int)Ter[i - 1][j - 1].getType() + (int)Ter[i + 1][j].getType() +
                            (int)Ter[i + 1][j - 1].getType();
                            sum /= 7;*/

                            int ran = random.Next(0, 100);
                            if (ran <= sum && trueLand == true)
                            {
                                Ter[i][j].setType((EnumTypesTerrain)(Ter[i][j].getType() - 5));
                            }
                            Ter[i][j].setMainType();
                        }
                    }
                }
            }
        }
    }
}
