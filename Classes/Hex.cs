﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapGenerator.Classes
{


    class Hex
    {
        private int RandomHigh = 1;
        private int WidthHex, HeightHex;
        private int CoordinateX;
        private int CoordinateY;
        private EnumTypesTerrain Type;
        private EnumTypesTerrain Condiment;
        private bool mainType = false;
        private bool field = false;

        public Hex() 
        {
        
        }
        public Hex(int CoordinateX, int CoordinateY, EnumTypesTerrain type, EnumTypesTerrain Condiment, int RandomHigh)
        {
            this.CoordinateX = CoordinateX;
            this.CoordinateY = CoordinateY;  
            this.Type = type;
            this.Condiment = Condiment;
            this.RandomHigh = RandomHigh;
            this.field = false;
        }

        public int setWidthHex(int newWidth)
        {
            WidthHex = newWidth;
            return WidthHex;
        }
        public int setHeightHex(int newHeight)
        {
            HeightHex = newHeight;
            return HeightHex;
        }

        public int setCoordinateX(int X)
        {
            CoordinateX = X;
            return CoordinateX;
        }
        public int setCoordinateY(int Y)
        {
            CoordinateY = Y;
            return CoordinateY;
        }

        public int setRandomHigh(int high)
        {
            this.RandomHigh = high;
            return this.RandomHigh;
        }
        public void setMainType()
        {
            mainType = true;
        }

        public void setType(EnumTypesTerrain type)
        {
            Type = type;
        }

        public void setType(int type)
        {
            if (type < 43)
                Type = EnumTypesTerrain.ocean;
            else
            {
                if (type < 55)
                    Type = EnumTypesTerrain.grass;
                else if (type < 65)
                    Type = EnumTypesTerrain.grass_hills;
                else
                    Type = EnumTypesTerrain.grass_mountain;
                setField();
            }
        }

        public void setField()
        {
            field = true;
        }

        public bool getField()
        {
            return field;
        }

        public int getWidthHex()
        {
            return WidthHex;
        }
        public int getHeightHex()
        {
            return HeightHex;
        }

        public EnumTypesTerrain getType()
        {
            return Type;
        }

        public EnumTypesTerrain getCondiment()
        {
            return Condiment;
        }

        public int getRandomHigh()
        {
            return RandomHigh;
        }

        public bool getMainType()
        {
            return mainType;
        }
    }
}
