﻿using MapGenerator.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MapGenerator
{
    public partial class Form1 : Form
    {
        public int width = 32, height = 16;
        Graphics grafic;    //  графический объект — некий холст
        Bitmap bitmap;  //  буфер для Bitmap-изображения
        //PictureBox pictureBox1 = new PictureBox( PictureBox.);
        Terrain terrain;

        int x = 0, y = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            terrain = new Terrain(width,height,2);
            bitmap = new Bitmap(pictureBox.Width, pictureBox.Height);  // с размерами
            grafic = Graphics.FromImage(bitmap);   // инициализация g
            SolidBrush bf = new SolidBrush(Color.White); // перекраска фона
            grafic.FillRectangle(bf, 0, 0, pictureBox.Width, pictureBox.Height);
            Image image = MapGenerator.Properties.Resources.Icon_terrain_grass_mountain;
            write(width, height);
            GenerateDesert Desert = new GenerateDesert(ref terrain, width, height);
            
        }


        private void write(int width, int height)
        {
            int flag = -42;
            for (int i = 1; i < height; i++)
            {
                for (int j = 1; j < width; j++)
                {
                    imageList.Draw(grafic, new Point(x, y), (int)terrain.getTerrain()[i][j].getType());
                    pictureBox.Image = bitmap;
                    x += 85;
                }
                x = 0;
                x += flag;
                if (flag == -42)
                    flag = 0;
                else
                    flag = -42;
                y += 73;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            write(width, height);
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {

        }

        void pictureBox_MouseWheel(object sender, MouseEventArgs e)
        {
                Size pSize = pictureBox.Size;
                //Size size;
                const double delta = 1.1;
                int deltaSizeX, deltaSizeY;
                if (e.Delta > 0)
                {
                    //size = new Size((int)(pSize.Width * delta), (int)(pSize.Height * delta));

                    // deltaSizeX = (int)(pSize.Width * delta) - (int)(pSize.Width);
                    //  deltaSizeY = (int)(pSize.Height * delta) - (int)(pSize.Height);
                    // pictureBox1.Bounds = new Rectangle((int)(deltaSizeX - Cursor.Position.X ), (int)(deltaSizeY - Cursor.Position.Y ), (int)(pSize.Width * delta), (int)(pSize.Height * delta));   
                    int width = (int)(bitmap.Width * delta);
                    int height = (int)(bitmap.Height * delta);
                    Rectangle R1 = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                    Rectangle R2 = new Rectangle(e.X - width / 2, e.Y - height / 2, width, height);
                    grafic.DrawImage(pictureBox.Image, R1, R2, GraphicsUnit.Pixel);
                    pictureBox.Image = bitmap;
                    pictureBox.Refresh();
                    pictureBox.Update();
                }
                else
                {
                    // size = new Size((int)(pSize.Width * delta), (int)(pSize.Height * delta));

                    //deltaSizeX = (int)(pSize.Width / delta) - (int)(pSize.Width);
                    // deltaSizeY = (int)(pSize.Height / delta) - (int)(pSize.Height);
                    // pictureBox1.Bounds = new Rectangle((int)(deltaSizeX - Cursor.Position.X ), (int)(deltaSizeY - Cursor.Position.Y ), (int)(pSize.Width / delta), (int)(pSize.Height / delta));
                    int width = (int)(bitmap.Width / delta);
                    int height = (int)(bitmap.Height / delta);
                    Rectangle R1 = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
                    Rectangle R2 = new Rectangle(e.X - width / 2, e.Y - height / 2, width, height);
                    grafic.DrawImage(pictureBox.Image, R1, R2, GraphicsUnit.Pixel);
                    pictureBox.Update();
                    pictureBox.Image = bitmap;

                }
                //if (size.Width >= 50 && size.Height >= 50)
                // {
                //pictureBox1.Size = size;
                //newArea = (size.Width * size.Height);
                //imgArea = (pictureBox.Image.Size.Width * pictureBox.Image.Size.Height);
                //  }
            }
    }
}
